import { expect } from "chai";
import { Demineur } from "../src/demineur";


describe('Demineur', () => {

    it('should be able click', () => {

        const demineur = new Demineur();

        const action = demineur.isClick(true);

        expect(action).to.be.true;

    })
    
})